//
//  Results.swift
//  StarWars
//
//  Created by Виктория on 05/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import Foundation

struct Results: Codable {
    
    let birthYear: String?
    let created: String?
    let edited: String?
    let eyeColor: String?
    let films: [String]?
    let gender: String?
    let hairColor: String?
    let height: String?
    let homeworld: String?
    let mass: String?
    let name: String?
    let skinColor: String?
    let species: [String]?
    let starships: [String]?
    let url: String?
    let vehicles: [String]?
 
    var peopleInfoArray: [[String?]] {
        return [[self.name], [self.gender], [self.homeworld], self.species ?? [], [self.birthYear]]
    }
}

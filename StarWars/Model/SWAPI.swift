//
//  SWAPI.swift
//  StarWars
//
//  Created by Виктория on 05/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import Foundation

struct SWAPI {
    
    private let baseURL = "https://swapi.co/api/"
    var films: String {
        return baseURL + "films/"
    }
    var people: String {
        return baseURL + "people/"
    }
    var planets: String {
        return baseURL + "planets/"
    }
    var species: String {
        return baseURL + "species/"
    }
    var starships: String {
        return baseURL + "starships/"
    }
    var vehicles: String {
        return baseURL + "vehicles/"
    }
    
    func getFilm(with id: Int) -> String {
        return self.films + String(id) + "/"
    }
    
    func getPeople(with id: Int) -> String {
        return self.people + String(id) + "/"
    }
}


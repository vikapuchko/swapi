//
//  PlanetsResults.swift
//  StarWars
//
//  Created by Виктория on 13/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import Foundation

struct ResultsPlanets: Codable {
    
    let climate: String?
    let created: String?
    let diameter: String?
    let edited: String?
    let films: [String]?
    let gravity: String?
    let name: String?
    let orbitalPeriod: String?
    let population: String?
    let residents: [String]?
    let rotationPeriod: String?
    let surfaceWater: String?
    let terrain: String?
    let url: String?
    
    var planetsInfoArray: [String?] {
        return [self.name, self.diameter, self.terrain, self.gravity, self.climate]
    }
}

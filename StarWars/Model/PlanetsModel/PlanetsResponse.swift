//
//  PlanetsResponse.swift
//  StarWars
//
//  Created by Виктория on 13/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import Foundation

struct PlanetsResponse: Codable {
    
    let count: Int?
    let next: String?
    let previous: String?
    let results: [ResultsPlanets]
}

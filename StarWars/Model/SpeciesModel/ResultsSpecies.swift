//
//  ResultsSpecies.swift
//  StarWars
//
//  Created by Виктория on 05/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import Foundation

struct ResultsSpecies: Codable {
    
    let averageHeight: String?
    let averageLifespan: String?
    let classification: String?
    let created: String?
    let designation: String?
    let edited: String?
    let eyeColors: String?
    let films: [String]?
    let homeworld: String?
    let language: String?
    let name: String?
    let skinColors: String?
    let url: String?
    
    var speciesArray: [String?] {
        return [self.name, self.created, self.classification, self.designation, self.eyeColors, self.classification]
    }
}

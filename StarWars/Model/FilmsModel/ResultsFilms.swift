//
//  ResultsFilms.swift
//  StarWars
//
//  Created by Виктория on 05/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import Foundation

struct ResultsFilms: Codable {
    
    let characters: [String]?
    let created: String?
    let director: String?
    let edited: String?
    let episodeId: Int?
    let openingCrawl: String?
    let planets: [String]?
    let producer: String?
    let releaseDate: String?
    let species: [String]?
    let starships: [String]?
    let title: String?
    let url: String?
    let vehicles: [String]?
    
    var filmInfoArray: [String?] {
        return [self.title, String(self.episodeId ?? 0), self.releaseDate, self.director, self.openingCrawl]
    }
}

//
//  FilmsResponse.swift
//  StarWars
//
//  Created by Виктория on 05/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import Foundation

struct FilmsResponse: Codable {
    
    let count: Int?
    let next: String?
    let previous: String?
    let results: [ResultsFilms]
}

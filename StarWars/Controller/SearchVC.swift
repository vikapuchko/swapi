//
//  SearchVC.swift
//  StarWars
//
//  Created by Виктория on 11/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import UIKit

class SearchVC: UITableViewController {
    
    var results: [Results]? = nil
    private let cellID = "Cell"
    let searchController = UISearchController(searchResultsController: nil)
    var peopleResponse: Response? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        self.swapiPeople()
        self.searchBar()
    }
    
    func searchBar() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        tableView.tableFooterView = UIView()
        navigationItem.largeTitleDisplayMode = .automatic
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.hidesSearchBarWhenScrolling = true
        navigationController?.navigationBar.barTintColor = .black
        navigationItem.title = "SWAPI"
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.yellow]
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.yellow]
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.barStyle = .black
        self.definesPresentationContext = true
        self.navigationItem.searchController = self.searchController
    }
    
    func swapiPeople() {
        let url = SWAPI().people
        guard let convert = URL(string: url) else { return }
        let request = URLRequest(url: convert)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            if let data = data {
                self.getJSON(data: data)
            }
            }.resume()
    }
    
    func getJSON(data: Data) {
        do {
            let decoder = JSONDecoder()
            let model = try decoder.decode(Response.self, from: data)
            self.peopleResponse = model
        } catch let parsingError {
            print(parsingError)
        }
    }

    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return results?.count ?? 0
        }
        return peopleResponse?.results.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.selectionStyle = .none
        let results: Results?
        if isFiltering {
            results = self.results?[indexPath.row]
        } else {
            results = peopleResponse?.results[indexPath.row]
        }
        cell.textLabel?.text = results?.name
        return cell
    }
}

extension SearchVC: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text ?? "")
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        results = peopleResponse?.results.filter({( results : Results) -> Bool in
            return results.name?.lowercased().contains(searchText.lowercased()) ?? true
        })
        tableView.reloadData()
    }
    
    var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
}


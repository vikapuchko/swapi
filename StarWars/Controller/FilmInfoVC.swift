//
//  FilmInfo.swift
//  StarWars
//
//  Created by Виктория on 12/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import UIKit

class FilmInfoVC: UIViewController {
    
    var film: ResultsFilms? = nil
    let titleForInfo = ["Title", "Edisode ID", "Release Date", "Director", "Opening Crawl"]
    
    let cellID = "FilmInfoCell"
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
}

extension FilmInfoVC: UITableViewDelegate, UITableViewDataSource {
    
    func configure() {
        view.addSubview(tableView)
        view.backgroundColor = .white
        tableViewConstraint()
    }
    
    func tableViewConstraint() {
        let safeAreaView = view.safeAreaLayoutGuide
        tableView.topAnchor.constraint(equalTo: safeAreaView.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: safeAreaView.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: safeAreaView.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: safeAreaView.trailingAnchor).isActive = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        if let film = self.film {
            let parameter = film.filmInfoArray[indexPath.section]
            cell.textLabel?.text = parameter
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        return titleForInfo[section]
    }
}

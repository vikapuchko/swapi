//
//  TabBarController.swift
//  StarWars
//
//  Created by Виктория on 11/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    let filmsVC = UINavigationController(rootViewController: FilmsViewController())
    let searchVC = UINavigationController(rootViewController: SearchVC())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filmsVC.tabBarItem = UITabBarItem(title: "Movies", image: UIImage(named: "sw"), tag: 1)
        searchVC.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 1)
        let controllers = [filmsVC, searchVC]
        viewControllers = controllers
    }
}

//
//  SpeciesVC.swift
//  StarWars
//
//  Created by Виктория on 13/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import UIKit

class SpeciesVC: UIViewController {
    
    var speciesResults: ResultsSpecies? = nil
    let speciesArray = ["Name", "Created", "Classification", "Designation", "Eye Colors", "Language"]
    let cellID = "SpeciesCell"
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
}

extension SpeciesVC {
    
    func configure() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        tableViewConstraint()
    }
    
    func tableViewConstraint() {
        let viewSafeArea = view.safeAreaLayoutGuide
        tableView.topAnchor.constraint(equalTo: viewSafeArea.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: viewSafeArea.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: viewSafeArea.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: viewSafeArea.trailingAnchor).isActive = true
    }
}

extension SpeciesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        if let speciesParametr = speciesResults?.speciesArray[indexPath.section] {
            cell.textLabel?.text = speciesParametr
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return speciesResults?.speciesArray.count ?? 0
    }
    
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        return speciesArray[section]
    }
}

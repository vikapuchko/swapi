//
//  PlanetsVC.swift
//  StarWars
//
//  Created by Виктория on 13/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import UIKit

class PlanetsVC: UIViewController {
    
    let titleForInfo = ["Name", "Diameter", "Terrain", "Gravity", "Climate"]
    let cellID = "PlanetsInfoCell"
    var planets: ResultsPlanets? = nil
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
}

extension PlanetsVC: UITableViewDelegate, UITableViewDataSource {
    
    func configure() {
        view.addSubview(tableView)
        view.backgroundColor = .white
        tableViewConstraint()
    }
    
    func tableViewConstraint() {
        let safeAreaView = view.safeAreaLayoutGuide
        tableView.topAnchor.constraint(equalTo: safeAreaView.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: safeAreaView.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: safeAreaView.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: safeAreaView.trailingAnchor).isActive = true
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        if let planetsParameter = planets?.planetsInfoArray[indexPath.section] {
            cell.textLabel?.text = planetsParameter
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.planets?.planetsInfoArray.count ?? 0
    }
    
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        return titleForInfo[section]
    }
}



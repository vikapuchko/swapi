//
//  Category.swift
//  StarWars
//
//  Created by Виктория on 12/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {
    
    var film: ResultsFilms? = nil
    let cellID = "CategoryCell"
    let cellText = ["Info", "Character"]
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
}

extension CategoryViewController {
    
    func configure() {
        view.addSubview(tableView)
        tableViewConstraint()
        view.backgroundColor = .white
    }
    
    func tableViewConstraint() {
        let viewSafeArea = view.safeAreaLayoutGuide
        tableView.topAnchor.constraint(equalTo: viewSafeArea.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: viewSafeArea.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: viewSafeArea.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: viewSafeArea.trailingAnchor).isActive = true
    }
}

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return cellText.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = cellText[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            let filmsInfoController = FilmInfoVC()
            filmsInfoController.film = self.film
            navigationController?.pushViewController(filmsInfoController, animated: true)
        } else {
            guard let characters = self.film?.characters else { return }
            self.loadCharacter(for: characters[0]) { (character) in
                let characterInfoVC = FilmCharacterVC()
                characterInfoVC.character = character
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else { return }
                    this.navigationController?.pushViewController(characterInfoVC, animated: true)
                }
            }
        }
    }
    
    func loadCharacter(for urlString: String,
                       with completionHandler: @escaping (Results) -> ()) {
        print(urlString)
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let data = data else { return }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            do {
                let character = try decoder.decode(Results.self, from: data)
                completionHandler(character)
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
}

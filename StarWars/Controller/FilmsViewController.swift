//
//  ViewController.swift
//  StarWars
//
//  Created by Виктория on 05/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import UIKit

class FilmsViewController: UIViewController {
    
    var films: FilmsResponse? = nil {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let this = self else { return }
                this.tableView.reloadData()
            }
        }
    }
    
    let cellID = "TableViewCell"
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        swapiFilms()
        filmRequest()
    }
    
    func filmRequest() {
        let urlString = SWAPI().getFilm(with: 1)
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = data else { return }
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            do {
                let model = try jsonDecoder.decode(ResultsFilms.self, from: data)
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func swapiFilms() {
        let url = SWAPI().films
        guard let convert = URL(string: url) else { return }
        let request = URLRequest(url: convert)
        URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            guard let strongSelf = self else { return }
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if let data = data {
                strongSelf.getJsonForFilms(data: data)
            }
        }.resume()
    }
    
    func getJsonForFilms(data: Data) {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let model = try decoder.decode(FilmsResponse.self, from: data)
            self.films = model
        } catch let parsingError {
            print(parsingError)
        }
    }
}

extension FilmsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return films?.results.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.accessoryType = .disclosureIndicator
        guard let items = self.films?.results[indexPath.row] else { return cell }
        cell.textLabel?.text = items.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let categoryViewController = CategoryViewController()
        categoryViewController.film = self.films?.results[indexPath.row]
        navigationController?.pushViewController(categoryViewController, animated: true)
    }
}

extension FilmsViewController {
    
    func configure() {
        view.addSubview(tableView)
        tableViewConstraint()
    }
    
    func tableViewConstraint() {
        let viewSafeArea = view.safeAreaLayoutGuide
        tableView.topAnchor.constraint(equalTo: viewSafeArea.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: viewSafeArea.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: viewSafeArea.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: viewSafeArea.trailingAnchor).isActive = true
    }
}

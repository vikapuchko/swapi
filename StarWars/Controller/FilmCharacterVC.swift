//
//  FilmCharacterViewController.swift
//  StarWars
//
//  Created by Виктория on 12/12/2018.
//  Copyright © 2018 Виктория. All rights reserved.
//

import UIKit

class FilmCharacterVC: UIViewController {
    
    var character: Results? = nil
    let titleForCharacter = ["Name", "Gender", "Homeworld", "Species", "Birth Year"]
    let cellID = "CharacterCell"
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
        peopleRequest()
    }
    
    func peopleRequest() {
        let urlString = SWAPI().getPeople(with: 1)
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = data else { return }
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            do {
                let model = try jsonDecoder.decode(Results.self, from: data)
            } catch {
                print(error.localizedDescription)
            }
            }.resume()
    }
}

extension FilmCharacterVC: UITableViewDelegate, UITableViewDataSource {
    
    func configure() {
        view.addSubview(tableView)
        view.backgroundColor = .white
        tableViewConstraint()
    }
    
    func tableViewConstraint() {
        let safeAreaView = view.safeAreaLayoutGuide
        tableView.topAnchor.constraint(equalTo: safeAreaView.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: safeAreaView.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: safeAreaView.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: safeAreaView.trailingAnchor).isActive = true
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return self.character?.peopleInfoArray[section].count ?? 0
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        if let characterParameters = self.character?.peopleInfoArray[indexPath.section] {
            cell.textLabel?.text = characterParameters[indexPath.row]
        }
        switch indexPath.section {
        case 2:
            cell.accessoryType = .disclosureIndicator
        case 3:
            cell.accessoryType = .disclosureIndicator
        default:
            break
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.character?.peopleInfoArray.count ?? 0
    }
    
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        return titleForCharacter[section]
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 2:
            guard let planets = self.character?.homeworld else { return }
            self.loadPlanets(for: planets, with: { (planets) in
                let planetsVC = PlanetsVC()
                planetsVC.planets = planets
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else { return }
                    this.navigationController?.pushViewController(planetsVC, animated: true)
                }
            })
        case 3:
            guard let species = self.character?.species else { return }
            self.loadSpecies(for: species[indexPath.row]) { (species) in
                let speciesVC = SpeciesVC()
                speciesVC.speciesResults = species
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else { return }
                    this.navigationController?.pushViewController(speciesVC, animated: true)
                }
            }
        default:
            break
        }
    }

    func loadPlanets(for urlString: String,
                     with completionHandler: @escaping (ResultsPlanets) -> ()) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let data = data else { return }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            do {
                let planet = try decoder.decode(ResultsPlanets.self, from: data)
                completionHandler(planet)
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func loadSpecies(for urlString: String,
                     with complitionHandler: @escaping (ResultsSpecies) -> ()) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            guard let data = data else { return }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            do {
                let species = try decoder.decode(ResultsSpecies.self, from: data)
                complitionHandler(species)
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
}
